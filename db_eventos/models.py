from django.db import models
from django.db.models import Q
from model_utils import Choices


ORDER_COLUMN_CHOICES = Choices(
    ('0', 'id'),
    ('1', 'evento'),
    ('2', 'inicio'),
    ('3', 'fin'),
    ('4', 'cantidad_invitados'),
)


class eventos(models.Model):
    evento = models.CharField(max_length=100)
    inicio = models.DateTimeField()
    fin = models.DateTimeField()
    cantidad_invitados = models.IntegerField()



    class Meta:
        db_table = 'eventos'
        ordering = ("evento",)
        verbose_name = "evento"
        verbose_name_plural = "eventos"


    def __str__(self):
        return self.evento


def query_libros_by_args(**kwargs):
    draw = int(kwargs.get('draw', None)[0])
    length = int(kwargs.get('length', None)[0])
    start = int(kwargs.get('start', None)[0])
    search_value = kwargs.get('search[value]', None)[0]
    order_column = kwargs.get('order[0][column]', None)[0]
    order = kwargs.get('order[0][dir]', None)[0]


    order_column = ORDER_COLUMN_CHOICES[order_column]
    # django orm '-' -> desc
    if order == 'desc':
        order_column = '-' + order_column


    queryset = eventos.objects.all()
    total = queryset.count()


    if search_value:
        queryset = queryset.filter(Q(id__icontains=search_value) |
                                        Q(evento__icontains=search_value) |
                                        Q(inicio__icontains=search_value) |
                                        Q(fin__icontains=search_value) |
                                        Q(cantidad_invitados__icontains=search_value))


    count = queryset.count()


    if length == -1:
        queryset = queryset.order_by(order_column)
    else:
        queryset = queryset.order_by(order_column)[start: start + length]


    return {
        'items': queryset,
        'count': count,
        'total': total,
        'draw': draw
    }
