from django.urls import path, include


from rest_framework.routers import DefaultRouter


from db_eventos.views import index, eventosViewSet


router = DefaultRouter()
router.register(r'eventos', eventosViewSet, basename='eventos')


urlpatterns = [
    path('', index, name='index'),
    path('api/', include(router.urls)),
]