from django.conf import settings


from rest_framework import serializers


from .models import eventos


class eventosSerializer(serializers.ModelSerializer):
    # Si su <field_name> se declara en su serializador con el parámetro required=False
    # Entonces este paso de validación no tendrá lugar si el campo no está incluido.
    inicio = serializers.DateTimeField(format=settings.DATE_FORMAT)
    fin = serializers.DateTimeField(format=settings.DATETIME_FORMAT)


    class Meta:
        model = eventos
        fields = "__all__"
