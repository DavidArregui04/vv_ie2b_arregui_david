# Instancia Evaluativa 2 "Validación y Verificación de Programas"

* **Apellido y Nombres** Arregui David Julian

## Breve descripcion del proyecto

Este proyecto consiste en el desarrollo de una API utilizando Django Rest Framework (DRF) para gestionar una colección de eventos. La API proporcionará endpoints para realizar operaciones como obtener los detalles de un evento, añadir un nuevo evento, actualizar un evento existente y eliminar un evento. Además, se incluye la creación de tests para garantizar el correcto funcionamiento de estos endpoints en diversas circunstancias.

## Clonar el repositorio

Dentro del repositorio tocar el boton "Code" y copiar la url HTTPS para clonar el repositorio

* **Url del proyecto** [Proyecto](https://gitlab.com/DavidArregui04/vv_ie2b_arregui_david.git)

Luego en el cmd ingresamos el siguiente comando

    Users\Documents>git clone https://gitlab.com/DavidArregui04/vv_ie2b_arregui_david.git

## Indicaciones para el despliegue del proyecto

### Creación del entorno virtual

Venv es una librería incluida de manera standard con Python. En adelante se utilizará "vv_ie2b_arregui_david" como nombre del entorno virtual, pero puede utilizarse cualquier otro.

    Users\Documents\vv_ie2b_arregui_david>cd ..
    Users\Documents>py -m venv vv_ie2b_arregui_david

### Activación del entorno virtual

El entorno se activa desde:

    Users\Documents\>cd vv_ie2b_arregui_david
    Users\Documents\vv_ie2b_arregui_david>cd scripts
    Users\Documents\vv_ie2b_arregui_david\Scripts>activate

Cuando se active aparecera asi:

    (vv_ie2b_arregui_david) Users\Documents\vv_ie2b_arregui_david\Scripts>

Luego regresar a la carpeta "vv_ie2b_arregui_david"

    (vv_ie2b_arregui_david) Users\Documents\vv_ie2b_arregui_david>

### Instalación de requerimientos

* **Actualizar el pip**

    (vv_ie2b_arregui_david) Users\Documents\vv_ie2b_arregui_david>python.exe -m pip install --upgrade pip

* **Instalar requerimientos**

    (vv_ie2b_arregui_david) Users\Documents\vv_ie2b_arregui_david>pip install -r requirements.txt

## Ejecución del servidor web

Dentro de nuestro virtual ingresamos lo siguiente:

    (vv_ie2b_arregui_david) Users\Documents\vv_ie2b_arregui_david>python manage.py runserver

## Ejecución del proyecto en el navegador

Ingresar al siguiente link

* **Proyecto en el navegador** [Host](http://127.0.0.1:8000/)
